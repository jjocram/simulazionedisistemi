/*
 * Servant.cpp
 *
 *  Created on: 22 mar 2021
 *      Author: marco
 */

#include "Servant.h"

Define_Module(Servant);

void Servant::initialize() {
    EV_INFO << "Initializing servant with id: " << getId() << std::endl;

    queueingTimeSignal = registerSignal("queueingTime");
    queueLengthSignal = registerSignal("queueLength");
    emit(queueLengthSignal, 0);
    busySignal = registerSignal("busy");
    emit(busySignal, false);

    servedJob = nullptr;
    servedJobStartTime = 0.0;
    finishProcessJobMessage = new cMessage("end-service");;
    queue.setName("queue");
}

void Servant::handleMessage(cMessage *msg) {

    if (msg == finishProcessJobMessage) {
        //EV_INFO << "Current job finished" << std::endl;
    // Received message scheduled
        endService(servedJob);

        if (queue.isEmpty()) {
            servedJob = nullptr;
            emit(busySignal, false);
        } else {
            servedJob = dynamic_cast<Job *>(queue.pop());
            emit(queueLengthSignal, queue.getLength());
            simtime_t serviceTime = startService(servedJob);
            scheduleAt(simTime() + serviceTime, finishProcessJobMessage);
        }

    }
    else {
    // Received a new message
        //EV_INFO << "Servant(" << getId()  << ") received a new job " << std::endl;
        Job* job = check_and_cast<Job*>(msg);

        job->setTimestamp(); // Job arrival time-stamp;
        job->setArrivalTime(simTime());

        if (servedJob) {
            //EV_INFO << "Servant is busy, job in the queue" << std::endl;
            // Servant is busy, put the job in the queue
            queue.insert(job);
            emit(queueLengthSignal, queue.getLength());
            job->setQueueCount(job->getQueueCount() + 1);
        } else {
            //EV_INFO << "Servant is in idle, start the job" << std::endl;
            // Servant is in idle, "start" the job
            servedJob = job;
            emit(busySignal, true);
            simtime_t serviceTime = startService(servedJob);
            //EV_INFO << "job will finish at: " << (simTime() + serviceTime) << std::endl;
            scheduleAt(simTime()+serviceTime, finishProcessJobMessage);
        }
    }
}

simtime_t Servant::startService(Job *job) {
    EV_INFO << "Starting service of " << job->getName() << std::endl;
    servedJobStartTime = simTime();
    simtime_t d = simTime() - job->getTimestamp();
    emit(queueingTimeSignal, d);
    job->setTotalQueueingTime(job->getTotalQueueingTime() + d);
    EV << "Starting service of " << job->getName() << endl;
    job->setTimestamp();
    return job->getTotalServiceTime();
}

void Servant::endService(Job *job) {
    EV_INFO << "Finishing service of " << job->getName() << std::endl;
    simtime_t d = simTime() - job->getTimestamp();
    job->setTotalServiceTime(job->getTotalServiceTime() + d);
    send(job, "out");
}

double Servant::getWorkload() {
    double totalWorkload = 0.0;
    for (cQueue::Iterator it(queue); !it.end(); it++) {
      Job *job = check_and_cast<Job*>(*it);
      totalWorkload = totalWorkload + job->getTotalServiceTime().dbl();
    }

    if (servedJob) {
        totalWorkload = totalWorkload + (servedJob->getTotalServiceTime().dbl() - (simTime().dbl() - servedJobStartTime.dbl())); // totalServiceTime of the current job minus the time spent on the current job
    }

    return totalWorkload;
}

Servant::~Servant() {
    delete servedJob;
    cancelAndDelete(finishProcessJobMessage);
}
