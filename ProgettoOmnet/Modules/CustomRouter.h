/*
 * CustomRouter.h
 *
 *  Created on: 22 mar 2021
 *      Author: marco
 */

#ifndef MODULES_CUSTOMROUTER_H_
#define MODULES_CUSTOMROUTER_H_

#include <omnetpp.h>
#include "Modules/Servant.h"
#include <Job.h>
#include <Router.h>

using namespace omnetpp;
using namespace queueing;

class CustomRouter : public Router {
private:
    simsignal_t dropped;
    int numberOfServant;
    int chooseServant(Job *job);
    bool isServantAvailable(Servant *servant, double &serviceTime, double &totalTimePrevision);
    Servant *servant1;
    Servant *servant2;


protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

#endif /* MODULES_CUSTOMROUTER_H_ */
