/*
 * CustomRouter.cpp
 *
 *  Created on: 22 mar 2021
 *      Author: Marco Ferrati
 */

#include "CustomRouter.h"

Define_Module(CustomRouter);

bool CustomRouter::isServantAvailable(Servant *servant, double &serviceTime, double &totalTimePrevision){
    double deadline = par("deadline");
    serviceTime = servant->par("serviceTime");
    totalTimePrevision = servant->getWorkload() + serviceTime;
    return totalTimePrevision <= deadline;
}

int CustomRouter::chooseServant(Job *job){
    if (numberOfServant == 1){
        double serviceTimeServant1;
        double totalTimePrevisionServant1;
        bool servant1Available = isServantAvailable(servant1, serviceTimeServant1, totalTimePrevisionServant1);
        job->setTotalServiceTime(serviceTimeServant1);
        return servant1Available ? 0 : -1;
    } else if (numberOfServant == 2) {
        double serviceTimeServant1, serviceTimeServant2;
        double totalTimePrevisionServant1, totalTimePrevisionServant2;

        bool servant1Available = isServantAvailable(servant1, serviceTimeServant1, totalTimePrevisionServant1);
        bool servant2Available = isServantAvailable(servant2, serviceTimeServant2, totalTimePrevisionServant2);

        if (!servant1Available && !servant2Available)
            return -1; // Both servant can't handle this job, this job will be discarded

        if (servant1Available && !servant2Available) {
            // Servant1 is available but not Servant2 -> send to servant1
            job->setTotalServiceTime(serviceTimeServant1);
            return 0;
        }

        if (!servant1Available && servant2Available) {
            // Servant1 is not available, Servant2 is available -> send to Servant2
            job->setTotalServiceTime(serviceTimeServant2);
            return 1;
        }

        if (servant1Available && servant2Available) {
            // Both are available
            if (totalTimePrevisionServant1 > totalTimePrevisionServant2) {
                // Servant1 will be faster
                job->setTotalServiceTime(serviceTimeServant1);
                return 0;
            } else {
                // Servant2 will be faster or equal
                job->setTotalServiceTime(serviceTimeServant2);
                return 1;
            }
        }
    }

    EV_ERROR << "Logic error in chooseServant, job will be discarded FIX THE PROBLEM!" << std::endl;
    return -1;
}

void CustomRouter::initialize() {
    numberOfServant = par("numberOfServant").intValue();
    dropped = registerSignal("dropped");
    EV_INFO << "Initialized the router: there are " << numberOfServant << " servant" << std::endl;

    if (numberOfServant == 1) {
        servant1 = dynamic_cast<Servant*>(getSimulation()->getModuleByPath("Network1Servant.servant1"));
        if (!servant1) {std::cout << "Servant1 not found" << std::endl; exit(0);}
    } else if (numberOfServant == 2) {
        servant1 = dynamic_cast<Servant*>(getSimulation()->getModuleByPath("Network2Servant.servant1"));
        servant2 = dynamic_cast<Servant*>(getSimulation()->getModuleByPath("Network2Servant.servant2"));
        if (!servant1 || !servant2) {std::cout << "Servant1 or Servant2 not found" << std::endl; exit(0);}
    } else {
        EV_ERROR << "Number of servant not accepted (only 1 or 2)" << std::endl;
        exit(0);
    }
}

void CustomRouter::handleMessage(cMessage *msg) {
    int servantChoosen = chooseServant(check_and_cast<Job*>(msg));
    EV_INFO << "Sending job through gate " << servantChoosen << std::endl;

    if (servantChoosen == -1) {
        EV_INFO << "Discarded job" << std::endl;
        emit(dropped, 1);
        delete msg;
        return;
    }

    send(msg, "out", servantChoosen);
}
