/*
 * Servant.h
 *
 *  Created on: 22 mar 2021
 *      Author: Marco Ferrati
 */

#ifndef MODULES_SERVANT_H_
#define MODULES_SERVANT_H_

#include <omnetpp.h>
#include <Job.h>

using namespace omnetpp;
using namespace queueing;

class Servant : public cSimpleModule {
private:
    simsignal_t busySignal; // true or false, used to signal when the servant is busy and when it changes it state
    simsignal_t queueLengthSignal;
    simsignal_t queueingTimeSignal;

    cQueue queue; // jobs queue
    Job *servedJob; // job actually served
    simtime_t servedJobStartTime; // simulation time when the served job begin to be served
    cMessage *finishProcessJobMessage; // This message is self-send when a job terminates to be processed
protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;

    simtime_t startService(Job *job);
    void endService(Job *job);
public:
    virtual ~Servant();
    double getWorkload();
};

#endif /* MODULES_SERVANT_H_ */
