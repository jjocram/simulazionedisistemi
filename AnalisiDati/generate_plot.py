import matplotlib.pyplot as plt

label = ["Media dati sperimentali", "Media dati legge di Little"]

medie_1_servente = [0.88917, 0.60667]
lb_1_servente = [0.77531, 0.48119]
ub_1_servente = [1.00303, 0.73216]

medie_2_servente = [0.79894, 0.83726]
lb_2_servente = [0.68109, 0.69679]
ub_2_servente = [0.91679, 0.97773]

plt.errorbar(label,
             medie_1_servente,
             yerr=[lb_1_servente, ub_1_servente],
             fmt='o')
plt.title("Modello a 1 servente")
plt.savefig('convalida1servente.png')

plt.clf()

plt.errorbar(label,
             medie_2_servente,
             yerr=[lb_2_servente, ub_2_servente],
             fmt='o')
plt.title("Modello a 2 serventi")
plt.savefig('convalida2servente.png')

